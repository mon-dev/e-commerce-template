(function ($, window, document, undefined) {
  'use strict';
  $(function () {
    // init sliders
    $('.slider').slick({
      arrows: true,
      prevArrow: '<button type="button" class="slick-prev"><img src="./assets/img/slider-left.jpg"></button>',
      nextArrow: '<button type="button" class="slick-next"><img src="./assets/img/slider-right.jpg"></button>'
    });
    $('.logos').slick({
      arrows: true,
      prevArrow: '<button type="button" class="slick-prev"><img src="./assets/img/slider-left.jpg"></button>',
      nextArrow: '<button type="button" class="slick-next"><img src="./assets/img/slider-right.jpg"></button>',
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 750,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
          }
        },
      ]
    });

    // init modal 
    $('#modal').iziModal({
      title: 'Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean',
      padding: 10,
      headerColor: '#1abc9c'
    });

    //Mobile menu button
    $(".img, .navigation").hide();
    $('.bar-btn').click(function () {
      $(".logo").css('max-height', '100%');
      $(".img, .navigation").css({ visibility: 'visible' }).slideToggle("slow");
    });
  });


})(jQuery, window, document);
